module chainmaker.org/chainmaker/chainmaker-net-liquid

go 1.15

require (
	chainmaker.org/chainmaker/chainmaker-net-common v0.0.5
	chainmaker.org/chainmaker/common/v2 v2.0.1-0.20210906085649-78f6202d8d60
	chainmaker.org/chainmaker/pb-go/v2 v2.0.0
	chainmaker.org/chainmaker/protocol/v2 v2.0.1-0.20210906092203-47d66f4908f7
	github.com/cznic/mathutil v0.0.0-20181122101859-297441e03548
	github.com/gogo/protobuf v1.3.2
	github.com/libp2p/go-yamux/v2 v2.2.0
	github.com/multiformats/go-multiaddr v0.3.2
	github.com/multiformats/go-multiaddr-fmt v0.1.0
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/tjfoc/gmsm v1.4.1
	github.com/xiaotianfork/q-tls-common v0.0.10
	github.com/xiaotianfork/quic-go v0.21.20
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c
)
